---
title: "Ansible Strikes Back"
date: 2017-09-23T09:15:10-04:00
tags:
  - web
  - automation
  - ansible
  - git
---

All Kinds of cool solutions today:

## Alternative Today I Learned.

<https://til.hashrocket.com/>

Aside from just looking really slick, one really nice thing about this is that
each post is limited to 200 words. I may adopt that here because some of these
have grown to prodigious, if not quite novel lengths.

##  Ansible FTW

The [other day][o] I dove way into some `bash` foo. Turns out that was all
unnecessary now that I know about the [`raw`][r] and [`script`][s] modules for
`ansible`.


[o]:{{< ref "2017-09-21.md" >}}
[s]:http://docs.ansible.com/ansible/latest/script_module.html
[r]:http://docs.ansible.com/ansible/latest/raw_module.html

## altssh FTW

Was faced with a situation where a machine had a relatively restrictive firewall
policy, and didn't want to have to resort to HTTPS git (long story the machine
also has an old git version that does not have [git-credential-cache][gcc]).

Turns out this is not a new problem.  Without further ado I present:

[GitLab][gl]:
```
Host gitlab.com
  Hostname altssh.gitlab.com
  User git
  Port 443
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/gitlab
```

[GitHub][gh]:
```
Host github.com
  Hostname ssh.github.com
  Port 443
```

[gcc]:https://git-scm.com/docs/git-credential-cache
[gl]:https://about.gitlab.com/2016/02/18/gitlab-dot-com-now-supports-an-alternate-git-plus-ssh-port/
[gh]:https://help.github.com/articles/using-ssh-over-the-https-port/
