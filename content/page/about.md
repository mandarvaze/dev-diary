---
title: "About"
date: 2017-09-08T14:37:23-04:00
---

This is a log of development related things I learned or didn't understand each
day.

This is inspired by a combination of Dean Wilson's [Development Diaries blog
post][bp], Daniel Reeves of Beeminder's [1000 Days of User-Visible
Improvements][uvi] and Paul Graham's philosophy of ["How Not To Die"][pg].

Disclaimer: The logs entries are largely unedited, rough cuts, and primarily for
my own reference and reflection. Think more scratch notepade and less fancy
blog. They are posted public on the off chance something useful falls out or
perhaps a kind soul has an answer/reference for something I didn't understand.
Also this is a small step to get in the habit of just shipping something.  The
source is available on [Gitlab][].

I welcome any questions, comments or recommendations at <roy@ragsdale.xyz>.

[bp]:https://www.unixdaemon.net/career/developer-diaries-and-til/
[uvi]:http://messymatters.com/uvi/
[pg]:http://www.paulgraham.com/die.html
[gitlab]:https://gitlab.com/royragsdale/dev-diary
